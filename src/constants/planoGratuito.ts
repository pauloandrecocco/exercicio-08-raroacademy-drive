import { PlanoDTO } from "../@types/dto/PlanoDto";

export const PLANO_GRATUITO: PlanoDTO = {
  "nome": "Plano Gratuito",
  "valor": 0,
  "limiteGB": 5
}