import { RequestUsuario } from "../@types/middlewares/requestUsuario";
import { NextFunction, Response } from "express";
import { BadRequestError } from "../@types/errors/BadRequestError";
import { PADRAO_SENHA } from "../constants/padraoSenha";

export const middlewareValidacaoCriacaoUser = (request: RequestUsuario, response: Response, next: NextFunction) => {
  const { nome, email, senha } = request.body;

  if(!nome || !email || !senha) {
    throw new BadRequestError();
  }

  if(!PADRAO_SENHA.test(senha)) {
    throw new BadRequestError();
  }

  next();
}

export const middlewareValidacaoCriacaoPlano = (request: RequestUsuario, response: Response, next: NextFunction) => {
  const { nome, valor, limiteGB } = request.body;

  if(!nome || valor === undefined || valor < 0 || !limiteGB || limiteGB < 0) {
    throw new BadRequestError();
  }

  next();
}