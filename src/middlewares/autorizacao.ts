import { RequestUsuario } from "../@types/middlewares/requestUsuario";
import { NextFunction, Response } from "express";
import { ForbiddenError } from "../@types/errors/ForbiddenError";

export const middlewareAutorizacao = (request: RequestUsuario, response: Response, next: NextFunction) => {
  const { usuario } = request;
  if (!usuario.root) {
    throw new ForbiddenError();
  }

  next();
}
