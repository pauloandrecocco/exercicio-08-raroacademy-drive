import { RequestUsuario } from "../@types/middlewares/requestUsuario";
import { NextFunction, Response } from "express";
import { verificarToken } from "../helpers/Token";
import { UnauthorizedError } from "../@types/errors/UnauthorizedError";

export const middlewareValidacaoToken = (request: RequestUsuario, response: Response, next: NextFunction) => {
  try {
    const { params: { token } } = request;
    verificarToken(token);
  } catch (error) {
    throw new UnauthorizedError();
  }

  next();
}
