import { Plano } from '../models/PlanoEntity';
import { EntityRepository, Like, Repository } from 'typeorm';
import { IPlanoRepository } from '../@types/repositories/IPlanoRepository';

@EntityRepository(Plano)
export class PlanoRepository
  extends Repository<Plano>
  implements IPlanoRepository
{
  criar(plano: Plano): Promise<Plano> {
    return this.save(plano);
  }

  atualizar(id: string, plano: Partial<Plano>): Promise<Plano> {
    return this.save({ id, ...plano });
  }

  async remover(id: string): Promise<void> {
    await this.softDelete(id);
  }

  listar(): Promise<Plano[]> {
    return this.find();
  }

  buscar(id: string): Promise<Plano> {
    return this.findOne(id);
  }

  buscarPorNome(nome: string): Promise<Plano> {
    return this.findOne({ where: { nome: Like(`%${nome}`) }});
  }
}
