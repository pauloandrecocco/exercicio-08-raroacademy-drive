import { Arquivo } from '../models/ArquivoEntity';
import { EntityRepository, ILike, Repository } from 'typeorm';
import { IArquivoRepository } from '../@types/repositories/IArquivoRepository';
import { QueryArquivos } from '../@types/dto/ArquivoDto';

@EntityRepository(Arquivo)
export class ArquivoRepository
  extends Repository<Arquivo>
  implements IArquivoRepository {
  criar(arquivo: Arquivo): Promise<Arquivo> {
    return this.save(arquivo);
  }

  atualizar(id: string, arquivo: Partial<Arquivo>): Promise<Arquivo> {
    return this.save({ id, ...arquivo });
  }

  async remover(id: string): Promise<void> {
    await this.softDelete(id);
  }

  listar(query: QueryArquivos): Promise<Arquivo[]> {
    const { usuarioId, nome, tipo, page, per, orderBy, orderDirection } = query;

    return this.find({
      where: {
        nome: ILike(`%${nome}%`),
        tipo: ILike(`%${tipo}%`),
        usuario: { id: usuarioId },
      },
      order: {
        [orderBy]: orderDirection
      },
      skip: (page - 1) * per,
      take: per,
    });
  }

  listarSemPaginacao(usuarioId: string): Promise<Arquivo[]> {
    return this.find({
      where: {
        usuario: { id: usuarioId },
      },
    });
  }

  contar(queryArquivos: QueryArquivos) {
    const { usuarioId, nome, tipo } = queryArquivos;

    return this.count({
      where: {
        nome: ILike(`%${nome}%`),
        tipo: ILike(`%${tipo}%`),
        usuario: { id: usuarioId },
      }
    });
  }

  buscar(id: string): Promise<Arquivo> {
    return this.findOne({
      where: { id },
      relations: ['usuario']
    });
  }

  buscarPorCaminho(caminho: string): Promise<Arquivo> {
    return this.findOne({
      where: { caminho },
      relations: ['usuario']
    });
  }
}
