import { IUsuarioRepository } from "../@types/repositories/IUsuarioRepository";
import { Usuario } from "../models/UsuarioEntity";
import { EntityRepository, In, Repository } from "typeorm";
import { CadastroUsuarioDTO } from "../@types/dto/AutenticacaoDto";

@EntityRepository(Usuario)
export class UsuarioRepository extends Repository<Usuario> implements IUsuarioRepository
  {
    async cadastrar(usuario: CadastroUsuarioDTO): Promise<Usuario> {
      return this.save(usuario);
    }

    async buscar(email: string): Promise<Usuario> {
      return await this.findOne({
        where: { email }
      })
    }

    async buscarPorEmails(emails: string[]): Promise<Usuario[]> {
      return await this.find({
        where: { email: In(emails) }
      })
    }
  }
