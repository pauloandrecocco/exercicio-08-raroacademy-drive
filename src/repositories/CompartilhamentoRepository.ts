import { Compartilhamento } from '../models/CompartilhamentoEntity';
import { EntityRepository, Repository, LessThanOrEqual, MoreThanOrEqual, getRepository } from 'typeorm';
import { ICompartilhamentoRepository } from '../@types/repositories/ICompartilhamentoRepository';

@EntityRepository(Compartilhamento)
export class CompartilhamentoRepository
  extends Repository<Compartilhamento>
  implements ICompartilhamentoRepository {
  async listar(usuarioId: string): Promise<Compartilhamento[]> {
    const compartilhamentoRepo = getRepository(Compartilhamento)
    return compartilhamentoRepo.find({
      relations: [ 'usuario', 'arquivo' ],
      where: [
        {
          usuario: { id: usuarioId },
          dataInicio: LessThanOrEqual(new Date()),
          dataFim: null
        },
        {
          usuario: { id: usuarioId },
          dataInicio: LessThanOrEqual(new Date()),
          dataFim: MoreThanOrEqual(new Date())
        }
      ]
    });
  }

  async buscar(id: string): Promise<Compartilhamento> {
    return this.findOne({
      where: { id },
      relations: ['usuario', 'arquivo']
    });
  }

  criar(compartilhamento: Compartilhamento): Promise<Compartilhamento> {
    return this.save(compartilhamento);
  }
}
