import { Assinatura } from '../models/AssinaturaEntity';
import { EntityRepository, Repository } from 'typeorm';
import { IAssinaturaRepository } from '../@types/repositories/IAssinaturaRepository';

@EntityRepository(Assinatura)
export class AssinaturaRepository
  extends Repository<Assinatura>
  implements IAssinaturaRepository
{
  criar(assinatura: Assinatura): Promise<Assinatura> {
    return this.save(assinatura);
  }

  alterar(id: string, assinatura: Partial<Assinatura>): Promise<Assinatura> {
    return this.save({ id, ...assinatura });
  }

  buscar(id: string): Promise<Assinatura> {
    return this.findOne(id);
  }

  buscarPorUsuario(id: string): Promise<Assinatura> {
    return this.findOne({
      where: { usuario: { id } },
      relations: ["plano"],
    });
  }
}