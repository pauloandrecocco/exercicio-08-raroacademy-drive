import { ArquivoDTO, QueryArquivos, RetornoListaArquivos, RetornoUploadArquivo } from '../dto/ArquivoDto';
import { Arquivo } from '../../models/ArquivoEntity';

export interface IArquivoService {
  listar(queryArquivos: QueryArquivos): Promise<RetornoListaArquivos>;
  buscar(id: string, usuarioId: string): Promise<Arquivo>;
  buscarPorToken(token: string): Promise<Arquivo>;
  gerarLink(id: string, usuarioId: string, validade: string): Promise<string>;
  upload(arquivoDto: ArquivoDTO): Promise<RetornoUploadArquivo>;
  atualizar(id: string, arquivoDto: ArquivoDTO): Promise<void>;
  remover(id: string, usuarioId: string): Promise<void>;
}
