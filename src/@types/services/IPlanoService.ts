import { PlanoDTO } from '../dto/PlanoDto';
import { Plano } from '../../models/PlanoEntity';

export interface IPlanoService {
  listar(): Promise<Plano[]>;
  buscar(id: string): Promise<Plano>;
  buscarPorNome(nome: string): Promise<Plano>;
  criar(planoDto: PlanoDTO): Promise<Plano>;
  atualizar(id: string, planoDto: PlanoDTO): Promise<void>;
  remover(id: string): Promise<void>;
}
