import { RetornoAutenticarDTO, RetornoUsuarioDTO } from "../../@types/dto/AutenticacaoDto";
import { Usuario } from "../../models/UsuarioEntity";

export interface IAutenticacaoService {
  autenticar(login: string, senha: string): Promise<RetornoAutenticarDTO>;
  cadastrarUsuario(usuario: Usuario): Promise<RetornoUsuarioDTO>;
}