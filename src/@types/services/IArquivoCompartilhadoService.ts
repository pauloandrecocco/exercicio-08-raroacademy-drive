import { CompartilhamentoArquivoDTO } from '../../@types/dto/ArquivoDto';
import { Arquivo } from '../../models/ArquivoEntity';

export interface IArquivoCompartilhadoService {
  listar(usuarioId: string): Promise<Arquivo[]>;
  compartilhar(compartilhamentoArquivoDTO: CompartilhamentoArquivoDTO): Promise<void>;
}
