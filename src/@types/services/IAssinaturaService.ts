import { AssinaturaDTO } from '../dto/AssinaturaDto';
import { Assinatura } from '../../models/AssinaturaEntity';

export interface IAssinaturaService {
  criar(assinaturaDto: AssinaturaDTO): Promise<Assinatura>;
  alterar(id: string, assinaturaDto: AssinaturaDTO): Promise<void>;
  buscar(id: string): Promise<Assinatura>;
  buscarPorUsuario(usuarioId: string): Promise<Assinatura>;
  trocarPlano(id: string, assinaturaDto: AssinaturaDTO): Promise<void>;
}
