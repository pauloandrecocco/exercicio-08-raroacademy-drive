import { Repository } from "typeorm";
import { Compartilhamento } from "../../models/CompartilhamentoEntity";

export interface ICompartilhamentoRepository extends Repository<Compartilhamento> {
  criar(compartilhamento: Compartilhamento): Promise<Compartilhamento>;
  listar(usuarioId: string): Promise<Compartilhamento[]>;
  buscar(id: string): Promise<Compartilhamento>;
}
