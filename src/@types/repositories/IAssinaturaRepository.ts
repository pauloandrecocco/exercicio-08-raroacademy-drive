import { AssinaturaDTO } from "../../@types/dto/AssinaturaDto";
import { Repository } from "typeorm";
import { Assinatura } from "../../models/AssinaturaEntity";

export interface IAssinaturaRepository extends Repository<Assinatura> {
  criar(assinatura: AssinaturaDTO): Promise<Assinatura>;
  alterar(id: string, assinatura: Partial<Assinatura>): Promise<Assinatura>;
  buscar(id: string): Promise<Assinatura>;
  buscarPorUsuario(usuarioId: string): Promise<Assinatura>;
}
