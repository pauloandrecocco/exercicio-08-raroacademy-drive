import { Repository } from 'typeorm';
import { CadastroUsuarioDTO } from '../../@types/dto/AutenticacaoDto';
import { Usuario } from '../../models/UsuarioEntity';

export interface IUsuarioRepository extends Repository<Usuario> {
  cadastrar(usuario: CadastroUsuarioDTO): Promise<Usuario>;
  buscar(email: string): Promise<Usuario>;
  buscarPorEmails(emails: string[]): Promise<Usuario[]>;
}
