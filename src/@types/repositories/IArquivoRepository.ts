import { QueryArquivos } from '../../@types/dto/ArquivoDto';
import { Arquivo } from '../../models/ArquivoEntity';

export interface IArquivoRepository {
  criar(arquivo: Arquivo): Promise<Arquivo>;
  atualizar(id: string, arquivo: Partial<Arquivo>): Promise<Arquivo>;
  remover(id: string): Promise<void>;
  listar(query: QueryArquivos): Promise<Arquivo[]>;
  listarSemPaginacao(usuarioId: string): Promise<Arquivo[]>;
  contar(query: QueryArquivos): Promise<number>;
  buscar(id: string): Promise<Arquivo>;
  buscarPorCaminho(caminho: string): Promise<Arquivo>;
}
