import { PlanoDTO } from '../../@types/dto/PlanoDto';
import { Plano } from '../../models/PlanoEntity';

export interface IPlanoRepository {
  criar(plano: PlanoDTO): Promise<Plano>;
  atualizar(id: string, plano: Partial<Plano>): Promise<Plano>;
  remover(id: string): Promise<void>;
  listar(): Promise<Plano[]>;
  buscar(id: string): Promise<Plano>;
  buscarPorNome(nome: string): Promise<Plano>;
}
