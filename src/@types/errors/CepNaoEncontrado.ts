import { NotFoundError } from './NotFoundError';

export class CepNaoEncontrado extends NotFoundError {
  public name: string;
  constructor() {
    super('CEP não encontrado');
    this.name = 'CepNaoEncontrado';
  }
}
