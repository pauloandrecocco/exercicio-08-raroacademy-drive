import { Plano } from "../../models/PlanoEntity";

export class PlanoDTO {
  nome: string;
  valor: number;
  limiteGB: number;
}
