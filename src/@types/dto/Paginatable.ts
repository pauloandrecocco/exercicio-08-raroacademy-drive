export interface PaginatableQuery {
  page?: number;
  per?: number;
  orderBy?: string;
  orderDirection?: string;
}
