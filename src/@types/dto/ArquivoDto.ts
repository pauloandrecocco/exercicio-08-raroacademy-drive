import { ListMetadata } from "../../@types/utils/ListMetadata";
import { Pagination } from "../../@types/utils/Pagination";
import { Arquivo } from "../../models/ArquivoEntity";

export class ArquivoDTO extends Arquivo {}

export class RetornoListaArquivos {
  data: Arquivo[];
  meta: ListMetadata;
}

export interface QueryArquivos extends Pagination {
  usuarioId: string;
  nome?: string;
  tipo?: string;
}

export interface RetornoUploadArquivo {
  id: string;
  usuarioId: string;
  nome: string;
  tipo: string;
}

export interface CompartilhamentoArquivoDTO {
  arquivoId: string;
  emails: string[];
  dataFim?: Date;
  token?: string;
}
