import { Plano } from "../../models/PlanoEntity";
import { Usuario } from "../../models/UsuarioEntity";

export interface AssinaturaDTO {
  usuario: Usuario;
  plano: Plano;
  dataInicio: Date;
}

export interface AssinaturaTrocaPlanoDTO extends AssinaturaDTO {
  token: string;
}