export interface LoginDTO {
  nome: string;
  senha: string;
}

export interface CadastroUsuarioDTO {
  nome: string;
  email: string;
  senha: string;
}

export interface TokenUsuarioDTO {
  id: string;
  root: boolean;
}

export interface RetornoUsuarioDTO {
  id: string;
  nome: string;
  email: string;
}

export interface RetornoAutenticarDTO {
  token: string
  isRoot: boolean
}
