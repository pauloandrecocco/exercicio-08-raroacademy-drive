import createDatabaseConnection from "../config/database/connect";
import createDependencyInjector from "../config/dependencies/createInjector";
import Container from "typedi";
import { UsuarioSeeder } from "./seeders/UsuarioSeeder";

export const run = async () => {
  try {
    await createDatabaseConnection();
    createDependencyInjector();

    const seeders = [
      Container.get<UsuarioSeeder>('UsuarioSeeder'),
    ];

    console.log('start seeding')
    for (const seeder of seeders) {
      await seeder.run();
    }

    console.log('seeding done');
    process.exit(0);
  } catch (error) {
    console.error('Fatal error: ', error);
    process.exit(1);
  }
};

run();
