import { IUsuarioRepository } from "../../@types/repositories/IUsuarioRepository";
import { Inject, Service } from "typedi";
import { Seeder } from "./Seeder";
import { Usuario } from "../../models/UsuarioEntity";
import { plainToInstance } from "class-transformer";
import { IAutenticacaoService } from "../../@types/services/IAutenticacaoService";

@Service('UsuarioSeeder')
export class UsuarioSeeder implements Seeder {
  constructor(
    @Inject('UsuarioRepository') private usuarioRepository: IUsuarioRepository,
    @Inject('AutenticacaoService') private autenticacaoService: IAutenticacaoService
  ) {}

  async run(): Promise<void> {
    const dadosAdmin = {
      nome:  process.env.ADMIN_USER_NAME,
      email: process.env.ADMIN_USER_MAIL,
      senha: process.env.ADMIN_USER_PASS,
      root: true
    }

    const admin = await this.usuarioRepository.findOne({
      where: {
        root: true
      }
    })

    if (!admin) {
      await this.autenticacaoService.cadastrarUsuario(plainToInstance(Usuario, dadosAdmin));
    }
  }
}
