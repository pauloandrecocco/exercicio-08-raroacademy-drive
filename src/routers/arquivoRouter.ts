import { Router } from 'express';
import { middlewareAutenticacao } from '../middlewares/autenticacao';
import { middlewareValidacaoToken } from '../middlewares/validacaoToken';
import Container from 'typedi';
import { ArquivoController } from '../controllers/ArquivoController';
import { errorHandlerWrapper } from '../middlewares/errorHandlers';
import * as multer from 'multer';
import { RequestUsuario as Response } from '../@types/middlewares/requestUsuario';
import { ArquivoCompartilhadoController } from '../controllers/ArquivoCompartilhadoController';

const router = Router();
const crateRouter = () => {
  const upload = multer({ dest: './uploads' });
  const arquivoController = Container.get<ArquivoController>('ArquivoController');
  const arquivoCompartilhadoController = Container.get<ArquivoCompartilhadoController>(
    'ArquivoCompartilhadoController'
  );

  // Não pode ter a autenticação de usuário, só a validação do token
  router.get('/:token/downloadPublico', middlewareValidacaoToken, errorHandlerWrapper((req: Response, res) => arquivoController.downloadPublico(req, res)));

  // Rotas que precisam de autenticação
  router.use(middlewareAutenticacao);
  router.post('',
    upload.single('file'),
    errorHandlerWrapper((req: Response, res) => arquivoController.upload(req, res))
  );
  router.get('', errorHandlerWrapper((req: Response, res) => arquivoController.listar(req, res)));
  router.get('/:id/download', errorHandlerWrapper((req: Response, res) => arquivoController.download(req, res)));
  router.delete('/:id', errorHandlerWrapper((req: Response, res) => arquivoController.remover(req, res)));

  router.get('/shared', errorHandlerWrapper((req: Response, res) => arquivoCompartilhadoController.listar(req, res)));
  router.post('/:id/share', errorHandlerWrapper((req: Response, res) => arquivoCompartilhadoController.compartilhar(req, res)));

  router.get('/:id/gerar-link/:validade', errorHandlerWrapper((req: Response, res) => arquivoController.gerarLink(req, res)));

  return router;
};

export default crateRouter;
