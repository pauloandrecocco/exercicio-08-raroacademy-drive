import { Router } from 'express';
import { middlewareAutenticacao } from '../middlewares/autenticacao';
import Container from 'typedi';
const router = Router();
import { AssinaturaController } from '../controllers/AssinaturaController';
import { middlewareAutorizacao } from '../middlewares/autorizacao';
import { errorHandlerWrapper } from '../middlewares/errorHandlers';

const getController = (): AssinaturaController => {
  return Container.get<AssinaturaController>('AssinaturaController');
};

const createRouter = () => {
  const controller = getController();

  router.use(middlewareAutenticacao);
  router.post('', errorHandlerWrapper((req, res) => controller.criar(req, res)));
  router.patch('/:id', errorHandlerWrapper((req, res) => controller.alterar(req, res)));
  router.get('/:id', errorHandlerWrapper((req, res) => controller.buscar(req, res)));
  router.get('/usuario/:id', errorHandlerWrapper((req, res) => controller.buscarPorUsuario(req, res)));
  router.patch('/trocarplano/:id', errorHandlerWrapper((req, res) => controller.trocarPlano(req, res)));

  return router;
};

export default createRouter;
