import { Router } from 'express';
import { middlewareAutenticacao } from '../middlewares/autenticacao';
import Container from 'typedi';
const router = Router();
import { PlanoController } from '../controllers/PlanoController';
import { middlewareAutorizacao } from '../middlewares/autorizacao';
import { errorHandlerWrapper } from '../middlewares/errorHandlers';
import { middlewareValidacaoCriacaoPlano } from '../middlewares/validacaoCamposCriacao';

const getController = (): PlanoController => {
  return Container.get<PlanoController>('PlanoController');
};

const crateRouter = () => {
  const controller = getController();

  router.get('', errorHandlerWrapper((req, res) => controller.listar(req, res)));
  router.post(
    '',
    middlewareAutenticacao,
    middlewareAutorizacao,
    middlewareValidacaoCriacaoPlano,
    errorHandlerWrapper((req, res) => controller.criar(req, res))
  );
  router.get('/:id', errorHandlerWrapper((req, res) => controller.buscar(req, res)));
  router.patch('/:id', middlewareAutenticacao, errorHandlerWrapper((req, res) => controller.atualizar(req, res)));
  router.delete('/:id', middlewareAutenticacao, errorHandlerWrapper((req, res) => controller.remover(req, res)));
  router.get('/nome/:nome', errorHandlerWrapper((req, res) => controller.buscarPorNome(req, res)));

  return router;
};

export default crateRouter;
