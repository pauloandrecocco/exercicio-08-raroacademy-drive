import { Router } from 'express';
import { errorHandlerWrapper } from '../middlewares/errorHandlers';
import Container from 'typedi';
const router = Router();
import { AutenticacaoController } from '../controllers/AutenticacaoController';
import { middlewareValidacaoCriacaoUser } from '../middlewares/validacaoCamposCriacao';

const getController = (): AutenticacaoController => {
  return Container.get<AutenticacaoController>('AutenticacaoController');
};

const crateRouter = () => {
  const controller = getController();
  router.post('/signup', middlewareValidacaoCriacaoUser, errorHandlerWrapper((req, res) => controller.cadastrar(req, res)));
  router.post('/login', errorHandlerWrapper((req, res) => controller.autenticar(req, res)));
  return router;
};

export default crateRouter;
