import * as express from 'express';
import createPlanoRouter from './planoRouter';
import createAutenticacaoRouter from './autenticacaoRouter';
import createArquivoRouter from './arquivoRouter';
import createAssinaturaRouter from './assinaturaRouter';

const createRouters = (app: express.Express) => {
  app.use('/planos', createPlanoRouter());
  app.use('/auth', createAutenticacaoRouter());
  app.use('/arquivos', createArquivoRouter());
  app.use('/assinaturas', createAssinaturaRouter());
};

export default createRouters;
