import { getCustomRepository } from 'typeorm';
import Container from 'typedi';
import { UserRepository } from '../../repositories/UserRepository';
import { PlanoRepository } from '../../repositories/PlanoRepository';
import { UsuarioRepository } from '../../repositories/UsuarioRepository';
import { ArquivoRepository } from '../../repositories/ArquivoRepository';
import { CompartilhamentoRepository } from '../../repositories/CompartilhamentoRepository';
import { AssinaturaRepository } from '../../repositories/AssinaturaRepository';

// inicializador de dependências:
// inicializa controllers
import '../../controllers/UserController';
import '../../controllers/EnderecoController';
import '../../controllers/PlanoController';
import '../../controllers/AutenticacaoController'
import '../../controllers/ArquivoController'
import '../../controllers/ArquivoCompartilhadoController'
import '../../controllers/AssinaturaController'

// inicializa services
import '../../services/UserService';
import '../../services/EnderecoService';
import '../../services/PlanoService';
import '../../services/AutenticacaoService'
import '../../services/ArquivoService'
import '../../services/ArquivoCompartilhadoService'
import '../../services/AssinaturaService'

// inicializa clientes
import '../../clients/CepClient';
import '../../infra/http/AxiosHttpClient';

// inicializa seeds
import '../../database/seeders/UsuarioSeeder';

const createDependencyInjector = () => {
  Container.set('UserRepository', getCustomRepository(UserRepository));
  Container.set('PlanoRepository', getCustomRepository(PlanoRepository));
  Container.set('UsuarioRepository', getCustomRepository(UsuarioRepository));
  Container.set('ArquivoRepository', getCustomRepository(ArquivoRepository));
  Container.set('CompartilhamentoRepository', getCustomRepository(CompartilhamentoRepository));
  Container.set('AssinaturaRepository', getCustomRepository(AssinaturaRepository));
};

export default createDependencyInjector;
