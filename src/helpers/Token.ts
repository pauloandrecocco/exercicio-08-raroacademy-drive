import { sign, verify } from "jsonwebtoken";
import { RetornoAutenticarDTO, TokenUsuarioDTO } from "../@types/dto/AutenticacaoDto";

export const verificarToken = (token: string): TokenUsuarioDTO => {
  return verify(token, process.env.AUTH_SECRET) as TokenUsuarioDTO;
}
export const gerarToken = (usuario: TokenUsuarioDTO): RetornoAutenticarDTO => {
  const token = sign(usuario, process.env.AUTH_SECRET);
  return { token, isRoot: usuario.root };
}

export const gerarTokenCompartilhamentoArquivo = (arquivoId: string, validade: string) => {
  const token = sign({arquivoId}, process.env.AUTH_SECRET, { expiresIn: validade });
  return { token };
}