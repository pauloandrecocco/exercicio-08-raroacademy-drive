import { Type } from 'class-transformer';
import { IsDefined, IsNumber, IsString, IsUUID } from 'class-validator';
import {
  Column,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Compartilhamento } from './CompartilhamentoEntity';
import { LinkCompartilhamento } from './LinkCompartilhamentoEntity';
import { Usuario } from './UsuarioEntity';

@Entity()
export class Arquivo {
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Usuario, (usuario) => usuario.arquivos)
  @Type(() => Usuario)
  @IsDefined()
  usuario: Usuario;

  @IsString()
  @IsDefined()
  @Column()
  nome: string;

  @IsString()
  @IsDefined()
  @Column()
  tipo: string;

  @IsString()
  @IsDefined()
  @Column()
  caminho: string;

  @IsNumber()
  @IsDefined()
  @Column()
  tamanho: number;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(
    () => Compartilhamento,
    (compartilhamento) => compartilhamento.arquivo
  )
  compartilhamentos: Compartilhamento[];

  @OneToMany(
    () => LinkCompartilhamento,
    (linkCompartilhado) => linkCompartilhado.arquivo
  )
  linksCompartilhados: LinkCompartilhamento[];
}
