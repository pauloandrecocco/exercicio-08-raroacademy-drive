import { IsDefined, IsString, IsUUID } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Arquivo } from './ArquivoEntity';
import { Assinatura } from './AssinaturaEntity';
import { Compartilhamento } from './CompartilhamentoEntity';
import { LinkCompartilhamento } from './LinkCompartilhamentoEntity';

@Entity()
export class Usuario {
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsString()
  @IsDefined()
  @Column()
  nome: string;

  @IsString()
  @IsDefined()
  @Column()
  email: string;

  @IsString()
  @IsDefined()
  @Column()
  senha: string;

  @IsString()
  @IsDefined()
  @Column({ default: false })
  root: boolean;

  @OneToMany(() => Assinatura, (assinatura) => assinatura.usuario)
  assinaturas: Assinatura[];

  @OneToMany(() => Arquivo, (arquivo) => arquivo.usuario)
  arquivos: Arquivo[];

  @OneToMany(
    () => Compartilhamento,
    (compartilhamento) => compartilhamento.usuario
  )
  compartilhamentos: Compartilhamento[];

  @OneToMany(
    () => LinkCompartilhamento,
    (linkCompartilhado) => linkCompartilhado.usuario
  )
  linksCompartilhados: LinkCompartilhamento[];
}
