import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsUUID } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Arquivo } from './ArquivoEntity';
import { Usuario } from './UsuarioEntity';

@Entity()
export class LinkCompartilhamento {
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Arquivo, (arquivo) => arquivo.linksCompartilhados)
  @Type(() => Arquivo)
  @IsDefined()
  arquivo: Arquivo;

  @ManyToOne(() => Usuario, (usuario) => usuario.linksCompartilhados)
  @Type(() => Usuario)
  @IsDefined()
  usuario: Usuario;

  @IsDate()
  @IsDefined()
  @Column({ name: 'data_inicio' })
  @Type(() => Date)
  dataInicio: Date;

  @IsDate()
  @Column({ name: 'data_fim', nullable: true })
  @Type(() => Date)
  dataFim: Date;
}
