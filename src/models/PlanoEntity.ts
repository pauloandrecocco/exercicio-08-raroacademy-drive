import { IsDefined, IsString, IsUUID, Min } from 'class-validator';
import {
  Column,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Assinatura } from './AssinaturaEntity';

@Entity()
export class Plano {
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsString()
  @IsDefined()
  @Column()
  nome: string;

  @Min(0)
  @Column()
  valor: number;

  @Min(0)
  @Column({ name: 'limite_gb' })
  limiteGB: number;

  @OneToMany(() => Assinatura, (assinatura) => assinatura.plano)
  assinaturas: Assinatura[];

  @DeleteDateColumn()
  deletedAt: Date;
}
