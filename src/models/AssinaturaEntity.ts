import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsUUID } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Plano } from './PlanoEntity';
import { Usuario } from './UsuarioEntity';

@Entity()
export class Assinatura {
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Usuario, (usuario) => usuario.assinaturas)
  @Type(() => Usuario)
  @IsDefined()
  usuario: Usuario;

  @ManyToOne(() => Plano, (plano) => plano.assinaturas)
  @Type(() => Plano)
  @IsDefined()
  plano: Plano;

  @IsDate()
  @IsDefined()
  @Column({ name: 'data_inicio' })
  @Type(() => Date)
  dataInicio: Date;

  @IsDate()
  @Column({ name: 'data_fim', nullable: true })
  @Type(() => Date)
  dataFim: Date;
}
