import { IAutenticacaoService } from '../@types/services/IAutenticacaoService';
import { Inject, Service } from 'typedi';
import { IUsuarioRepository } from '../@types/repositories/IUsuarioRepository';
import { UsuarioOuSenhaInvalidos } from '../@types/errors/UsuarioOuSenhaInvalidos';
import { hashSenha } from '../helpers/HashSenha';
import { CadastroUsuarioDTO, RetornoAutenticarDTO, RetornoUsuarioDTO } from '../@types/dto/AutenticacaoDto';
import { gerarToken } from '../helpers/Token';
import { EmailJaCadastrado } from '../@types/errors/EmailJaCadastrado';
import { IPlanoRepository } from '../@types/repositories/IPlanoRepository';
import { IAssinaturaRepository } from '../@types/repositories/IAssinaturaRepository';
import { PLANO_GRATUITO } from '../constants/planoGratuito';

@Service('AutenticacaoService')
export class AutenticacaoService implements IAutenticacaoService {
  constructor(
    @Inject('UsuarioRepository') private userRepository: IUsuarioRepository,
    @Inject('PlanoRepository') private planoRepository: IPlanoRepository,
    @Inject('AssinaturaRepository') private assinaturaRepository: IAssinaturaRepository
  ) {}

  async autenticar(email: string, senha: string): Promise<RetornoAutenticarDTO> {
    const usuarioCadastrado = await this.userRepository.buscar(email)

    if (!usuarioCadastrado) {
      throw new UsuarioOuSenhaInvalidos()
    }

    if (usuarioCadastrado.senha !== hashSenha(senha)) {
      throw new UsuarioOuSenhaInvalidos()
    }

    return gerarToken({
      id: usuarioCadastrado.id,
      root: usuarioCadastrado.root
    })
  }

  async cadastrarUsuario(usuario: CadastroUsuarioDTO): Promise<RetornoUsuarioDTO> {
    const hash = hashSenha(usuario.senha)
    const usuarioCadastrado = await this.userRepository.buscar(usuario.email)

    if (usuarioCadastrado) {
      throw new EmailJaCadastrado()
    }

    const novoUsuario = await this.userRepository.cadastrar({...usuario, senha: hash});

    // Inscreve automaticamente novo usuário no plano gratuito
    let planoGratuito = await this.planoRepository.buscarPorNome(PLANO_GRATUITO.nome);
    if (!planoGratuito) {
      planoGratuito = await this.planoRepository.criar(PLANO_GRATUITO);
    }
    await this.assinaturaRepository.criar({
      usuario: novoUsuario,
      plano: planoGratuito,
      dataInicio: new Date()
    });


    return {
      id: novoUsuario.id,
      email: novoUsuario.email,
      nome: novoUsuario.nome
    };
  }
}
