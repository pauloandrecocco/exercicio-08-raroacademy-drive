import { Inject, Service } from 'typedi';
import { ArquivoDTO, QueryArquivos } from '../@types/dto/ArquivoDto';
import { IArquivoService } from '../@types/services/IArquivoService';
import { IArquivoRepository } from '../@types/repositories/IArquivoRepository';
import { IAssinaturaRepository } from '../@types/repositories/IAssinaturaRepository';
import { NotFoundError } from '../@types/errors/NotFoundError';
import { CONVERSAO_GB_PARA_BYTES } from '../constants/gigabyteEmBytes';
import { token } from 'morgan';
import { gerarTokenCompartilhamentoArquivo } from '../helpers/Token';
import { Arquivo } from 'models/ArquivoEntity';
import jwt_decode from "jwt-decode";
import { linkAplicacao } from '../constants/linkPadrao';

@Service('ArquivoService')
export class ArquivoService implements IArquivoService {
  constructor(
    @Inject('ArquivoRepository') private arquivoRepository: IArquivoRepository,
    @Inject('AssinaturaRepository') private assinaturaRepository: IAssinaturaRepository
  ) {}

  async listar(queryArquivos: QueryArquivos) {
    const query = this.constroiQueryPadrao(queryArquivos);
    const arquivos = await this.arquivoRepository.listar(query);
    const total = await this.arquivoRepository.contar(query);

    return {
      data: arquivos,
      meta: {
        total,
        page: query.page,
        per: query.per,
      }
    }
  }

  async buscar(id: string, usuarioId: string) {
    const arquivo = await this.arquivoRepository.buscar(id);
    if (!arquivo) {
      throw new NotFoundError();
    }

    if (arquivo.usuario.id !== usuarioId) {
      throw new NotFoundError();
    }

    return arquivo;
  }

  async gerarLink(id: string, usuarioId: string, validade: string) {
    const arquivo = await this.arquivoRepository.buscar(id);
    if (!arquivo) {
      throw new NotFoundError();
    }

    if (arquivo.usuario.id !== usuarioId) {
      throw new NotFoundError();
    }

    // Criar token que retorna arquivo. id e validade
    const token = gerarTokenCompartilhamentoArquivo(arquivo.id, validade);

    return (linkAplicacao + 'arquivos/' + token.token + "/downloadPublico");
  }

  async buscarPorToken(token: string): Promise<Arquivo> {
    type tipoTokenDecoded = {
      arquivoId: string,
      iat: number,
      exp: number
    }
    const tokenDecoded: tipoTokenDecoded = jwt_decode(token);

    const arquivo = await this.arquivoRepository.buscar(tokenDecoded.arquivoId);
    if (!arquivo) {
      throw new NotFoundError();
    }

    return arquivo;
  }

  async upload(arquivoDto: ArquivoDTO) {
    await this.verificaTamanhoArquivo(arquivoDto);

    const { usuario, ...arquivo } = await this.arquivoRepository.criar(arquivoDto);
    return {
      usuarioId: usuario.id,
      ...arquivo
    };
  }

  async atualizar(id: string, arquivoDto: ArquivoDTO) {
    await this.arquivoRepository.atualizar(id, arquivoDto);
  }

  async remover(id: string, usuarioId: string) {
    const arquivoToRemove = await this.arquivoRepository.buscar(id);
    if (!arquivoToRemove) {
      throw new NotFoundError();
    }

    if (arquivoToRemove.usuario.id !== usuarioId) {
      throw new NotFoundError();
    }

    await this.arquivoRepository.remover(id);
  }

  private constroiQueryPadrao(query: QueryArquivos): QueryArquivos {
    return {
      usuarioId: query.usuarioId,
      nome: query.nome || '',
      tipo: query.tipo || '',
      page: query.page > 0 ? query.page : 1,
      per: query.per > 0 ? query.per : 20,
      orderBy: query.orderBy || 'nome',
      orderDirection: query.orderDirection || 'ASC',
    };
  }

  private async verificaTamanhoArquivo(arquivoDto: ArquivoDTO): Promise<void> {
    const tamanhoArquivo = arquivoDto.tamanho;
    
    const espacoTotal = (await this.assinaturaRepository.buscarPorUsuario(arquivoDto.usuario.id)).plano.limiteGB * CONVERSAO_GB_PARA_BYTES;

    const arquivosUsuario = await this.arquivoRepository.listarSemPaginacao(arquivoDto.usuario.id);
    const espacoUsado = arquivosUsuario.reduce((acumulador, arquivo) => {
      return acumulador + arquivo.tamanho;
    }, 0);

    if(tamanhoArquivo > (espacoTotal - espacoUsado)) {
      throw new Error("Tamanho do arquivo maior do que o disponível para o usuário.");
    }
  }
}
