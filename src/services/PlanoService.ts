import { Inject, Service } from 'typedi';
import { PlanoDTO } from '../@types/dto/PlanoDto';
import { IPlanoService } from '../@types/services/IPlanoService';
import { IPlanoRepository } from '../@types/repositories/IPlanoRepository';
import { NotFoundError } from '../@types/errors/NotFoundError';

@Service('PlanoService')
export class PlanoService implements IPlanoService {
  constructor(
    @Inject('PlanoRepository') private planoRepository: IPlanoRepository
  ) {}

  async listar() {
    return this.planoRepository.listar();
  }

  async buscar(id: string) {
    const plano = await this.planoRepository.buscar(id);
    if (!plano) {
      throw new NotFoundError();
    }

    return plano;
  }

  async buscarPorNome(nome: string) {
    const plano = await this.planoRepository.buscarPorNome(nome);
    if (!plano) {
      throw new NotFoundError();
    }

    return plano;
  }

  async criar(planoDto: PlanoDTO) {
    return this.planoRepository.criar(planoDto);
  }

  async atualizar(id: string, planoDto: PlanoDTO) {
    await this.planoRepository.atualizar(id, planoDto);
  }

  async remover(id: string) {
    const planoToRemove = await this.planoRepository.buscar(id);
    if (!planoToRemove) {
      throw new NotFoundError();
    }

    await this.planoRepository.remover(id);
  }
}
