import { Inject, Service } from 'typedi';
import { IAssinaturaService } from '../@types/services/IAssinaturaService';
import { NotFoundError } from '../@types/errors/NotFoundError';
import { IAssinaturaRepository } from '../@types/repositories/IAssinaturaRepository';
import { AssinaturaDTO, AssinaturaTrocaPlanoDTO } from '../@types/dto/AssinaturaDto';
import { IArquivoRepository } from '../@types/repositories/IArquivoRepository';
import { IPlanoService } from '../@types/services/IPlanoService';
import { CONVERSAO_GB_PARA_BYTES } from '../constants/gigabyteEmBytes';
import { padraoTokenDecoded } from '../constants/padraoToken';
import jwt_decode from "jwt-decode";

@Service('AssinaturaService')
export class AssinaturaService implements IAssinaturaService {
  constructor(
    @Inject('AssinaturaRepository') private assinaturaRepository: IAssinaturaRepository,
    @Inject('ArquivoRepository') private arquivoRepository: IArquivoRepository,
    @Inject('PlanoService') private readonly planoService: IPlanoService
  ) {}

  async criar(assinaturaDto: AssinaturaDTO) {
    return this.assinaturaRepository.criar(assinaturaDto);
  }
  
  async alterar(id: string, assinaturaDto: AssinaturaDTO) {
    await this.assinaturaRepository.alterar(id, assinaturaDto);
  }

  async buscar(id: string) {
    const assinatura = await this.assinaturaRepository.buscar(id);
    if (!assinatura) {
      throw new NotFoundError();
    }

    return assinatura;
  }

  async buscarPorUsuario(id: string) {
    const assinatura = await this.assinaturaRepository.buscarPorUsuario(id);
    if (!assinatura) {
      throw new NotFoundError();
    }

    return assinatura;
  }

  async trocarPlano(id: string, assinaturaDto: AssinaturaTrocaPlanoDTO) {
    const tokenDecoded: padraoTokenDecoded = jwt_decode(assinaturaDto.token);
    const usuarioId = tokenDecoded.id;

    await this.verificaPossibilidadeTrocaPlano(id, usuarioId);

    const assinaturaId = (await this.assinaturaRepository.buscarPorUsuario(usuarioId)).id;

    assinaturaDto.plano = (await this.planoService.buscar(id));
    delete assinaturaDto.token;

    await this.alterar(assinaturaId, assinaturaDto);
  }

  private async verificaPossibilidadeTrocaPlano(id: string, usuarioId: string): Promise<void> {
    const planoAtualId = (await this.assinaturaRepository.buscarPorUsuario(usuarioId)).plano.id;
    const planoDesejado = await this.planoService.buscar(id);
    if (planoAtualId === planoDesejado.id) {
      throw new Error('Usuário já está inscrito neste plano!')
    }

    const espacoPlanoDesejado = planoDesejado.limiteGB * CONVERSAO_GB_PARA_BYTES;
    // console.log(`O plano desejado possui disponível ${espacoPlanoDesejado} bytes`);

    const arquivosUsuario = await this.arquivoRepository.listarSemPaginacao(usuarioId);
    const espacoUsado = arquivosUsuario.reduce((acumulador, arquivo) => { return acumulador + arquivo.tamanho }, 0);
    // console.log(`Usuário utiliza ${espacoUsado} bytes`);

    if(espacoUsado > espacoPlanoDesejado) {
      throw new Error(`Impossível trocar de plano. Plano desejado comporta ${espacoPlanoDesejado} bytes. Usuário armazena ${espacoUsado} bytes`);
    }
  }
}
