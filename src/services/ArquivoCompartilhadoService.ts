import { Inject, Service } from 'typedi';
import { IArquivoCompartilhadoService } from '../@types/services/IArquivoCompartilhadoService';
import { ICompartilhamentoRepository } from '../@types/repositories/ICompartilhamentoRepository';
import { IArquivoRepository } from '../@types/repositories/IArquivoRepository';
import { NotFoundError } from '../@types/errors/NotFoundError';
import { IUsuarioRepository } from '../@types/repositories/IUsuarioRepository';
import { CompartilhamentoArquivoDTO } from '../@types/dto/ArquivoDto';
import { plainToInstance } from 'class-transformer';
import { Compartilhamento } from '../models/CompartilhamentoEntity';
import { padraoTokenDecoded } from '../constants/padraoToken';
import jwt_decode from "jwt-decode";

@Service('ArquivoCompartilhadoService')
export class ArquivoCompartilhadoService implements IArquivoCompartilhadoService {
  constructor(
    @Inject('CompartilhamentoRepository')
    private compartilhamentoRepository: ICompartilhamentoRepository,
    @Inject('ArquivoRepository')
    private arquivoRepository: IArquivoRepository,
    @Inject('UsuarioRepository')
    private usuarioRepository: IUsuarioRepository
  ) {}

  async listar(usuarioId: string) {
    const compartilhamentos = await this.compartilhamentoRepository.listar(usuarioId);
    return compartilhamentos.map(c => c.arquivo);
  }

  async compartilhar(
    compartilhamentoArquivoDTO: CompartilhamentoArquivoDTO
  ): Promise<void> {
    const {
      arquivoId,
      emails,
      dataFim,
    } = compartilhamentoArquivoDTO;
    
    const tokenDecoded: padraoTokenDecoded = jwt_decode(compartilhamentoArquivoDTO.token);
    const usuarioId = tokenDecoded.id;
    
    const arquivo = await this.arquivoRepository.buscarPorCaminho(arquivoId);
    if (arquivo.usuario.id !== usuarioId) {
      throw new NotFoundError();
    }

    const usuarios = await this.usuarioRepository.buscarPorEmails(emails);
    const promises = usuarios.map(usuario => {
      return this.compartilhamentoRepository.criar(
        plainToInstance(Compartilhamento, {
          arquivo,
          usuario,
          dataInicio: new Date(),
          dataFim
        })
      )
    });

    await Promise.all(promises);
  }
}
