import { Inject, Service } from 'typedi';
import { Request, Response } from 'express';
import { IAutenticacaoService } from '../@types/services/IAutenticacaoService';

@Service('AutenticacaoController')
export class AutenticacaoController {
  constructor(
    @Inject('AutenticacaoService') private autenticacaoService: IAutenticacaoService
  ) {}

  async autenticar(request: Request, response: Response) {
    const email = request.header('email');
    const senha = request.header('senha');
    const token = await this.autenticacaoService.autenticar(email, senha);
    response.status(200).send(token);
  }

  async cadastrar(request: Request, response: Response) {
    const usuario =  await this.autenticacaoService.cadastrarUsuario(request.body);
    response.status(201).send(usuario);
  }
}
