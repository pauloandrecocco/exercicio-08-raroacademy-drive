import { Inject, Service } from 'typedi';
import { Response } from 'express';
import { IArquivoCompartilhadoService } from '../@types/services/IArquivoCompartilhadoService';
import { RequestUsuario } from '../@types/middlewares/requestUsuario';

@Service('ArquivoCompartilhadoController')
export class ArquivoCompartilhadoController {
  constructor(
    @Inject('ArquivoCompartilhadoService')
    private readonly arquivoCompartilhadoService: IArquivoCompartilhadoService
  ) { }

  async listar(request: RequestUsuario, response: Response) {
    const arquivos = await this.arquivoCompartilhadoService.listar(request.usuario.id);
    response.send(arquivos);
  }

  async compartilhar(request: RequestUsuario, response: Response) {
    const token = String(request.headers['authorization'] || '');

    const arquivoId = request.params.id;
    const { emails, dataFim } = request.body;
    await this.arquivoCompartilhadoService.compartilhar({
      arquivoId,
      emails,
      dataFim,
      token
    });
    response.status(204).send();
  }
}
