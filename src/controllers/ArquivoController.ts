import { Inject, Service } from 'typedi';
import { Response } from 'express';
import { IArquivoService } from '../@types/services/IArquivoService';
import { RequestUsuario } from '../@types/middlewares/requestUsuario';
import { Arquivo } from '../models/ArquivoEntity';
import { plainToInstance } from 'class-transformer';
import { QueryArquivos } from '../@types/dto/ArquivoDto';

@Service('ArquivoController')
export class ArquivoController {
  constructor(@Inject('ArquivoService') private readonly arquivoService: IArquivoService) {}

  async listar(request: RequestUsuario, response: Response) {
    const arquivos = await this.arquivoService.listar(
      this.constroiQueryArquivo(request)
    );
    response.send(arquivos);
  }

  async gerarLink(request: RequestUsuario, response: Response) {
    const { usuario, params: { id, validade } } = request;
    const arquivoLink = await this.arquivoService.gerarLink(id, usuario.id, validade);

    response.send(arquivoLink);
  }

  async downloadPublico(request: RequestUsuario, response: Response) {
    const { params: { token } } = request;
    const arquivo = await this.arquivoService.buscarPorToken(token);

    response.setHeader('Content-disposition', `attachment; filename=${arquivo.nome}`);
    response.setHeader('Content-Type', arquivo.tipo);
    response.download(
      arquivo.caminho,
      arquivo.nome
    );
  }

  async upload(request: RequestUsuario, response: Response) {
    const { file, usuario } = request;
    const arquivoDto = plainToInstance(Arquivo, {
      usuario,
      nome: file.originalname,
      tipo: file.mimetype,
      caminho: file.path,
      tamanho: file.size
    });

    const arquivo = await this.arquivoService.upload(arquivoDto);
    response.status(201).send(arquivo);
  }

  async download(request: RequestUsuario, response: Response) {
    const { usuario, params: { id } } = request;
    const arquivo = await this.arquivoService.buscar(id, usuario.id);

    response.setHeader('Content-disposition', `attachment; filename=${arquivo.nome}`);
    response.setHeader('Content-Type', arquivo.tipo);
    response.download(
      arquivo.caminho,
      arquivo.nome
    );
  }

  async remover(request: RequestUsuario, response: Response) {
    const { usuario } = request;
    await this.arquivoService.remover(request.params.id, usuario.id);
    response.send();
  }

  private constroiQueryArquivo(request: RequestUsuario): QueryArquivos {
    return {
      usuarioId: request.usuario.id,
      nome: request.query.nome?.toString(),
      tipo: request.query.tipo?.toString(),
      page: Number(request.query.page?.toString() ?? 0),
      per: Number(request.query.per?.toString() ?? 0),
      orderBy: request.query.orderBy?.toString(),
      orderDirection: request.query.orderDirection?.toString(),
    }
  }
}
