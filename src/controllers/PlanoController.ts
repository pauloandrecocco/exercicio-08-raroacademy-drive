import { Inject, Service } from 'typedi';
import { Request, Response } from 'express';
import { IPlanoService } from '../@types/services/IPlanoService';

@Service('PlanoController')
export class PlanoController {
  constructor(@Inject('PlanoService') private readonly planoService: IPlanoService) {}

  async listar(request: Request, response: Response) {
    const planos = await this.planoService.listar();
    response.send(planos);
  }

  async buscar(request: Request, response: Response) {
    const plano = await this.planoService.buscar(request.params.id);
    response.send(plano);
  }

  async buscarPorNome(request: Request, response: Response) {
    const plano = await this.planoService.buscarPorNome(request.params.nome);
    response.send(plano);
  }

  async criar(request: Request, response: Response) {
    const plano = await this.planoService.criar(request.body);
    response.status(201).send(plano);
  }

  async atualizar(request: Request, response: Response) {
    const plano = await this.planoService.atualizar(
      request.params.id,
      request.body
    );
    response.send(plano);
  }

  async remover(request: Request, response: Response) {
    await this.planoService.remover(request.params.id);
    response.send();
  }
}
