import { Inject, Service } from 'typedi';
import { Request, Response } from 'express';
import { IAssinaturaService } from '../@types/services/IAssinaturaService';

@Service('AssinaturaController')
export class AssinaturaController {
  constructor(@Inject('AssinaturaService') private readonly assinaturaService: IAssinaturaService) {}
  
  async criar(request: Request, response: Response) {
    const assinatura = await this.assinaturaService.criar(request.body);
    response.status(201).send(assinatura);
  }
  
  async alterar(request: Request, response: Response) {
    const assinatura = await this.assinaturaService.alterar(
      request.params.id,
      request.body
    );
    response.send(assinatura);
  }
    
  async buscar(request: Request, response: Response) {
    const assinatura = await this.assinaturaService.buscar(request.params.id);
    response.send(assinatura);
  }

  async buscarPorUsuario(request: Request, response: Response) {
    const assinatura = await this.assinaturaService.buscarPorUsuario(request.params.id);
    response.send(assinatura);
  }

  async trocarPlano(request: Request, response: Response) {
    request.body.token = String(request.headers['authorization'] || '');
    const assinatura = await this.assinaturaService.trocarPlano(
      request.params.id,
      request.body
    );
    response.send(assinatura);
  }
}
