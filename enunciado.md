# raroacademy drive

Vamos construir um sistema de armazenamento e compartilhamento de arquivos, análogo ao Google Drive. Os seguintes recursos deverão ser observados para a construção desta plataforma:

## Gestão de conta
- Qualquer usuário poderá se cadastrar na plataforma. Para isto, deve-se preencher um formulário com nome, e-mail e senha. Validações necessárias:
  - Os três campos citados são obrigatórios
  - Um mesmo e-mail não pode ser utilizado para a criação de duas contas
  - A senha do usuário deverá ser salva em banco de dados, encriptado
  - _A senha do usuário precisa ter ao menos 8 caracteres, contendo letras maiúsculas, minúsculas, caracteres especiais e numéricos._
- Um usuário devidamente cadastrado poderá fazer login em nossa aplicação. Este método de login poderá ser utilizado para a geração do token, a ser utilizado pelo usuário

## Gestão de arquivos
- Os usuários poderão fazer o upload de arquivos para a nossa plataforma.
  - _Toda ação de upload precisa verificar se o usuário ainda tem espaço disponível, segundo seu plano contratado. Se não possuir, a ação de upload deverá ser negada._
- O usuário poderá listar todos seus arquivos, **mas somente seus arquivos**.
  - a lista deve ser paginada
  - o usuário poderá filtrar seus arquivos por nome e tipo.
- o usuário poderá listar também os arquivos que não são seus, mas que foram compartilhados com ele.
  - _Esta busca deve ter os mesmos critérios de paginação, ordenação e filtro da busca por todos os arquivos._
- O usuário somente poderá baixar seus próprios arquivos ou arquivos que foram compartilhados com ele.
- O usuário poderá deletar arquivos.
- O usuário poderá compartilhar um arquivo com outras pessoas, identificadas a partir de seu e-mail.
- _O usuário poderá também gerar um link de compartilhamento, que tornará o arquivo público por tempo determinado. Para isto, o usuário deverá selecionar o arquivo e o tempo que este arquivo poderá ser acessado publicamente. Esta ação deverá gerar um link, que poderá ser informado para qualquer pessoa, mesmo que não esteja logada na aplicação._


## Gestão de planos

- Obrigatoriamente, deve-se criar um plano gratuito, padrão para os usuários cadastrados
- Todo plano criado precisa ter um nome, um valor de assinatura mensal e o limite, em GB, de armazenamento habilitado para o usuário.
- O usuário root da aplicação poderá criar novas contas, editar e deletar contas.
- Qualquer usuário, inclusive usuários não autenticados, poderão listar os planos.

## Inscrição em planos
- _usuários logados na plataforma podem escolher um destes planos e se inscrever nele. A partir de então, seu limite de armazenamento será modificado. O usuário não pode fazer downgrade para um plano com limite de espaço menor que a quantidade de armazenamento atualmente utilizada._
